#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.

# SMTK License Version 1.0
# ========================================================================
# Copyright (c) 2012 Kitware Inc. 28 Corporate Drive
# Clifton Park, NY, 12065, USA.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.

#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

#  * Neither the name of Kitware nor the names of any contributors may
#    be used to endorse or promote products derived from this software
#    without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# The following files and directories come from third parties. Check the
# contents of these for details on the specifics of their respective
# licenses.
# - - - - - - - - - - - - - - - - - - - - - - - -
# Thirdparty/pugixml
# Thirdparty/PyYaml
# Thidpary/rtvl
# CMake/FindDocutils.cmake

import os
import posixpath
import json
from toposort import toposort_flatten


def template_args_to_underscores(name):
    if not name:
        return ""

    # First element is thing to replace, second is its substitute
    chars_to_replace = [("<", "_"), (", ", "_"), (">", ""), ("::", "_")]
    for old, new in chars_to_replace:
        name = name.replace(old, new)
    return name if len(name) > 1 else name[0]


# TODO: Could make a dictionary of class name to binding file.
# Then could loop through the keys, search for them in pygccxml data,
# and use the value to topological sort
def get_binding_order(classes):
    topological_mapping = dict()
    for obj in classes:
        dependencies = set()

        file_name = template_args_to_underscores(obj.name)
        for base_class in obj.recursive_bases:
            parent = base_class.related_class
            inherits = base_class.declaration_path
            # TODO
            # enable_shared = 'enable_shared_from_this<%s>' \
            #     % full_class_name(obj)

            if inherits != ["::", "std"]:
                dependencies.add(parent)
        topological_mapping[obj] = dependencies
    return toposort_flatten(topological_mapping)
