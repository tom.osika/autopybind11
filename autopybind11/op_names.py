# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

# Dictionary of C++ operator function symbols to python
# double underscore operators
# Some C++ operators have multiple python names depending on
# the number of arguments. For example, operator- is
# __neg__ (negation) if unary and __sub__ (subtraction) otherwise
names_dict = {
    "+": ["__pos__", "__add__"],
    "-": ["__neg__", "__sub__"],
    "*": "__mul__",
    "/": "__truediv__",
    "%": "__mod__",
    "&": "__and__",
    "&&": "__and__",
    "|": "__or__",
    "||": "__or__",
    "^": "__xor__",
    "^=": "__ixor__",
    "&=": "__iand__",
    "|=": "__ior__",
    "+=": "__iadd__",
    "-=": "__isub__",
    "*=": "__imul__",
    "/=": "__itruediv__",
    "()": "__call__",
    "==": "__eq__",
    "!=": "__neq__",
    "++": "__add__",
    "--": "__sub__",
    "[]": "__getitem__",
    "<": "__lt__",
    ">": "__gt__",
    "<=": "__le__",
    ">=": "__ge__",
    "%=": "__imod__",
    "bool": "__bool__",
    "!": "__bool__",
    "<<": "__lshift__",
    ">>": "__rshift__",
    "<<=": "__ilshift__",
    ">>=": "__irshift__",
}

# If the python name of an operator is dependent on the # of args,
# its symbol will be added here
arg_dependent_ops = ["+", "-"]
