# Class that helps with customization of C++ functions, enums, classes, etc.
class Customizer:

    def __init__(self, mod_tree):
        self.module_structure = mod_tree

    def fetch_customization_field(self, yaml_dict, field):
        if yaml_dict and "customization" in yaml_dict and yaml_dict["customization"]:
            cust_dict = yaml_dict["customization"]
            if field in cust_dict and cust_dict[field]:
                return cust_dict[field]
        return None

    # Accepts a yaml dictionary for a class, enum, etc. and
    # extracts the custom name from the customization field, if both exist.
    # Returns None if not found
    def get_custom_name(self, yaml_dict):
        name_field = self.fetch_customization_field(yaml_dict, "name")
        return name_field if name_field else None

    def get_custom_enum_vals(self, yaml_dict):
        enum_field = self.fetch_customization_field(yaml_dict, "custom_enum_vals")
        return enum_field if enum_field else {}

    def get_module_local_value(self, yaml_dict):
        if yaml_dict and "customization" in yaml_dict and yaml_dict["customization"]:
            if "module_local" in yaml_dict["customization"]:
                return True
        return False

    def get_blacklisted_members(self, yaml_dict):
        if yaml_dict and "customization" in yaml_dict and yaml_dict["customization"]:
            if "blacklist" in yaml_dict["customization"] \
                                        and yaml_dict["customization"]["blacklist"]:
                blacklist = yaml_dict["customization"]["blacklist"]
                return blacklist["functions"] + blacklist["members"]
        return []

    def get_curr_mod_name(self, yaml_dict):
        cust_namespace_structure = self.fetch_customization_field(yaml_dict, "namespace")
        if cust_namespace_structure:
            return cust_namespace_structure.split('.')[-1]
        return None

    # If c++ object is given a custom namespace location
    # extract absolute namespace path, return namespace
    # and location in yaml representation of hierarchy
    def add_custom_namespace(self, custom_namespace):
        namespace_structure = custom_namespace.split('.')
        curr_nmspc = namespace_structure[-1]
        mod_structure = self.module_structure
        if not self.find_and_insert_namespace(mod_structure,
                                              iter(namespace_structure),
                                              curr_nmspc
                                              ):
            self.module_structure[curr_nmspc] = {}

    def get_add_custom_namespace(self, yaml_dict):
        cust_namespace = self.fetch_customization_field(yaml_dict, "namespace")
        if cust_namespace is not None:
            self.add_custom_namespace(cust_namespace)
        return cust_namespace.split('.')[-1] if cust_namespace else None

    # name_strucuture is a dict object representing current location
    # in custom namespace structure (initialized as module level)
    # address is an iterator over a list of namespace addresses
    # representing the absolute namespace path
    def find_and_insert_namespace(self, name_structure, address, lowest_address):
        try:
            try:
                nxt_adr = next(address)

            except StopIteration:
                # we have reached the end of this namespace declaration
                # without reaching an unknown namespace, return True
                # if address is not in the current namespace structure, return false
                # so we can add it
                return (lowest_address in name_structure)
            is_final = nxt_adr == lowest_address
            name_structure = name_structure[nxt_adr]
            if is_final:
                ret = True
            else:
                ret = self.find_and_insert_namespace(name_structure, address, lowest_address)
        except KeyError:
            if not is_final:
                # we've had a failure here, one part of the absolute namespace is wrong
                # report that fact
                ret = False
            else:
                # we've found the location of this new namespace within the larger structure,
                # add it, and report
                name_structure[nxt_adr] = {}
                ret = True
        return ret

    def get_structure(self):
        if not self.module_structure:
            return None
        return self.module_structure
