import os
from os.path import abspath, dirname, join, isfile
from shutil import copyfile, which
import subprocess
from tempfile import TemporaryDirectory

_CLANG_FORMAT_SETTINGS = join(
    dirname(abspath(__file__)), "data/clang-format.yml"
)


def maybe_format_file_in_place(file):
    """
    If clang-format is avaiable, reformat the file according to the settings
    under data/.

    :return: True if reformatted, False otherwise.
    :throws: RuntimeError if clang-format runs but does not return 0.
    """
    if not able_to_format():
        return False
    # Use temporary directory so that we can use our custom format file in a
    # complete isolated context.
    with TemporaryDirectory() as tmp_dir:
        tmp_file = join(tmp_dir, "file.cpp")
        os.symlink(file, tmp_file)
        os.symlink(_CLANG_FORMAT_SETTINGS, join(tmp_dir, ".clang-format"))
        result = subprocess.run(
            [_CLANG_FORMAT_BIN, "--style=file", "-i", tmp_file],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        if result.returncode != 0:
            raise RuntimeError(f"clang-format failed: {file}\n{result.stdout}")
        # N.B. clang-format-9 -i sometimes replaces symlinks with physical copies
        # of the file.
        if not os.path.islink(tmp_file):
            copyfile(tmp_file, file)
    return True


def able_to_format():
    """Returns True if clang-format is available."""
    return _CLANG_FORMAT_BIN is not None


def _get_clang_format_path():
    # Returns path to executable if available, or None otherwise.
    return which("clang-format-9")


_CLANG_FORMAT_BIN = _get_clang_format_path()
