# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

"""
Separate file for the text blocks to be formatted.
These strings will be formatted in generator.py, then used to
generate valid pybind11 binding code
"""

submodule_signature = """py::module {name} = m.def_submodule("{name}", "");"""
arg_val_cast = " = {type}({val})"
nullptr_arg_val = " = ({type}){val}"
common_cpp_body = """
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

{forwards}
PYBIND11_EXPORT void apb11_{name}_register_types(py::module &model)
{{
// make sure this module is only initialized once
static bool called = false;
if(called) {{ return; }}
called = true;

// initialize class for module {name}
{init_funs}
}};

PYBIND11_MODULE({name}, model)
{{
// First initialize dependent modules
// then this module.
{autobind_calls}
}}

"""
parent_module = "model.attr(\"__name__\")=\"{parent}.{name}\";"
forward_ending = """(py::module &model)"""
signature_ending = """(model)"""

init_fun_signature = """apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=signature_ending
)
init_fun_forward = """void apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=forward_ending
)
module_init_fun_signature = """apb11_{{module}}_register_types{ending};\n""".format(
    ending=signature_ending
)
module_init_fun_forward = """void apb11_{{module}}_register_types{ending};\n""".format(
    ending=forward_ending
)
submodule_init_fun_signature = (
    """apb11_{module}_{name}_register({module_var});\n"""
)
submodule_init_fun_forward = (
    """void apb11_{module}_{name}_register(py::module &{module_var});\n"""
)
init_fun_forward_non_void = """py::module apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=forward_ending
)
submodule_init_fun_signature_return = (
    """auto {mod_name} = apb11_{module}_{name}_register({module_var});\n"""
)
submodule_init_fun_forward_non_void = (
    """py::module& apb11_{module}_{name}_register(py::module &{module_var});\n"""
)
submodule_return = """return {name};"""

cppbody = """
#include "pybind11/operators.h"
#include "pybind11/pybind11.h"
#include "pybind11/stl.h"
{includes}


namespace py = pybind11;
//#include "drake/bindings/pydrake/documentation_pybind.h"
//#include "drake/bindings/pydrake/pydrake_pybind.h"

// namespace drake {{
// namespace pydrake {{
PYBIND11_EXPORT void apb11_{name}_register_types(py::module &m)
{{
  {class_info}
}}

// }}  // namespace pydrake
// }}  // namespace drake
"""
module_local = """, py::module_local()"""
class_info_body = """
  py::class_<{pyclass_args}{no_delete}> {name}(m, "{name}"{doc}{mod_loc});

    {enums}
    {name}{constructor}
    {funcs}
    {vars}
    {opers}
    ;
"""
pybind_overload_macro_args = """localType,
      {parent_alias},
      {cpp_fxn_name},
      {arg_str}
"""

tramp_override = """{fxn_sig} override
  {{
    using localType = {return_type};
    PYBIND11_OVERLOAD{pure}
    (
      {macro_args}
    );
  }}"""

trampoline_def = """class {tramp_name}
: public {class_decl}
{{
public:
  typedef {class_decl} {parent_alias};
  using {parent_alias}::{ctor_name};

  {virtual_overrides}
}};
"""

publicist_using_directives = """using {class_decl}::{fxn_name};"""

publicist_def = """class {publicist_name}
: public {class_decl}
{{
public:
  {using_directives}
}};
"""

class_module_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
{includes}
{trampoline_str}
{publicist_str}

namespace py = pybind11;
void apb11_{module}_{namespace}_register(py::module &m)
{{
  static bool called = false;
  if(called) {{ return; }}
  called = true;
  {defs}
}}
"""

non_class_module_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
{includes}
namespace py = pybind11;
{forwards}
void apb11_{module}_{namespace}_register(py::module &m)
{{
  {defs}
}}
"""

non_class_module_return_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
{includes}
namespace py = pybind11;
{forwards}
py::module apb11_{module}_{namespace}_register(py::module &m)
{{
  {defs}
  return {name};
}}
"""

member_func = (
    """{module}.def{static}(\"{fun_name}\", {fun_ref}{args}{doc}){ending}\n"""
)

constructor = """.def(py::init<{}>(){}{})\n"""
copy_constructor_tramp = """.def(py::init([]({tramp_arg_type} arg0) {{ return {trampname}(arg0); }}){doc})
{indent}.def(py::init([]({arg_type} arg0) {{ return {cname}(arg0); }}))\n"""
member_func_arg = """py::arg(\"{}\"){}"""
public_member_var = """.def_read{write}{static}(\"{var_name}\", {var_ref})\n"""
private_member_var = (
    """.def_property{readonly}{static}(\"{var_name}\", {var_accessors})\n"""
)
member_reference = "&{classname}::{member}"
overload_template = """static_cast<{decl_string}>({fun_ref})"""
operator_template = """.def({arg1} {symbol} {arg2})\n"""
call_template = """.def("__call__", []({arg_str}){{ {fn_call} }})\n"""

wrap_header = """{includes}\n{class_decs}\n{func_ptr_assigns}"""
enum_header = (
    """py::enum_<{class_name}>({module},\"{name}\",{type}, "{doc}")\n"""
)
enum_val = """.value(\"{short_name}\", {scoped_name}, \"{doc}\")\n"""
