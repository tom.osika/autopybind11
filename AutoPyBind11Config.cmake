# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
#
# FIND_PACKAGE(AutoPyBind11 REQUIRED
#
# This file will define the following variables
#   - CastXML_EXECUTABLE                 : Path to the CastXML executable
#

include(FetchContent)

set(AutoPyBind11_INSTALL "${AutoPyBind11_DIR}")
include("${AutoPyBind11_DIR}/AutoPyBind11.cmake")
