Wrapper Generator
==================

This repository contains the first set of Python code which could be used to
automatically generate pybind11 code from C++ files with proper annotation.

**CAUTION**: The content, structure, and API of this repository are still under
development.  No guarantees of backwards compatibility are given for the
functionality.

License
+++++++

This repository is distributed under the OSI-approved BSD 3-clause License.
See Copyright.txt_ for details.

Program execution
+++++++++++++++++

Prerequisites
---------------

All instructions below assume that a Python3 environment is available.

CMake
#####

The CMake-based portion of the tool requires CMake 3.12 or greater.
Download or build CMake: https://cmake.org/

CastXML
#######

A version of the CastXML tool is required for this program to run. CastXML is
available from PYPI_ and will be installed by the PIP tool at the time of
install for the other Python dependencies. A selection of binary distributions
can be found on the `data.kitware.com`_ website.

*Note*: Due to the variance of available versions of CastXML, we recommend that
the PIP installed version be the only version used.  That is, the only version
of CastXML that can be found on the PATH variable.

Python Libraries
################

The repository contains a ``setup.py`` file which can be provided to the
PIP program to install all required Python Libraries.  Execute::

    pip install -e .

to install the this library and other required programs.
It is **strongly** recommended to use this through a virtual enviroment - see
below.  If you do choose to use a virtual environment, we recommend adding
``-DPython_FIND_VIRTUALENV=FIRST`` to the ``cmake`` command.  This will
tell CMake to prefer the Python executable in the virtual environment over the
system libraries.

Running Tests
#############

Here's a small recipe for setting up local dependencies and building and
running the tests in a virtual environment:

.. code-block:: shell

    # In project root
    python3 -m virtualenv -p python3 ./venv/
    source ./venv/bin/activate
    pip install -e .

    # Configure with CMake
    mkdir build && cd build
    cmake -DPython3_FIND_VIRTUALENV=FIRST ..
    make -j

    # Run smoke test for regression on codegen first.
    ctest -V -R smoke_test

    # Run all tests.
    ctest -V -R

For a non-virtual environment, you would follow a script like below:

.. code-block::

    pip install -e .
    mkdir build && cd build
    cmake ..
    make -j
    ctest -V -R

Preparing C++ code
------------------

Selecting C++ Objects
######################
The setup for the C++ code to be wrapped is simple. Each C++ directory with content
to be wrapped should contain a file named ``wrapper_input.yml``.
This content should describe the classes, "free" functions
(functions that are not a part of a class), and non-typed enumerations.

This information follows a certain structure.  There are a few reserved keywords
+-----------------------------+---------------------------------+
|           Keyword           |      Expected Data              |
+=============================+=================================+
|          `files`            |  A set of C++ header files      |
|                             |  which contains the objects to  |
|                             |  be wrapped                     |
|                             |                                 |
|                             |  Placed Under:                  |
+-----------------------------+---------------------------------+
|         `namespaces`        |  A set of strings which are the |
|                             |  namespaces which contains the  |
|                             |  objects to be wrapped          |
|                             |                                 |
|                             |  Placed Under: `files`          |
|                             |                `namespaces`     |
+-----------------------------+---------------------------------+
|         `classes`           |  Descriptive information which  |
|                             |  describes the class object to  |
|                             |  be wrapped                     |
|                             |                                 |
|                             |  Placed Under: `files`          |
|                             |                `namespaces`     |
+-----------------------------+---------------------------------+
|         `functions`         |  Descriptive information which  |
|                             |  describes the function object  |
|                             |  to be wrapped                  |
|                             |                                 |
|                             |  Placed Under: `files`          |
|                             |                `namespaces`     |
+-----------------------------+---------------------------------+
|         `enums`             |  Descriptive information which  |
|                             |  describes the enumeration      |
|                             |  object to be wrapped           |
|                             |                                 |
|                             |  Placed Under: `files`          |
|                             |                `namespaces`     |
+-----------------------------+---------------------------------+
An skeleton of a properly formed YAML file can be seen below. To see additional
examples, see the `Tests` directory of this repository.

.. parsed-literal::

files:
  <file_name>:
    namespaces:
      <namespace>:
        classes:
          <class_name>:
            <class_data>
          <class_2_name>:
            <class_2_data>
        functions:
          <function_name>:
            <function_data>
    namespaces:
      <namespace_2>:
        enums:
          <enum_name>:
            <enum_data>

Each type of object within the reserved tags requires different pieces of information:

+-----------------------------+---------------------------------+
|           C++ Object        |      Required YAML pieces       |
+=============================+=================================+
|           Class             | inst: []                        |
|                             |   List of instantiation types   |
|                             |   for class.                    |
|                             |   Leave blank for non-templated |
+-----------------------------+---------------------------------+
|        Function             | is_template: <bool>             |
|                             |  "true" if function is template |
|                             |  "false" otherwise              |
|                             +---------------------------------+
|                             | inst: []                        |
|                             |   List of instantiation types   |
|                             |   for function, if is_template  |
|                             |   is true                       |
+-----------------------------+---------------------------------+
|        Enumeration          |  <No additional information>    |
+-----------------------------+---------------------------------+

An "empty" object, for example, the `simple` class or the enumeration `test`
in the following example are treated as non-templated objects::

  classes:
    simple:
  namespaces:
    first:
      enums:
        test:

Customize Output
#####################

Customization of the output of AutoPyBind11 can occur at two levels:
at the code level and at the object level.

Object Customization
%%%%%%%%%%%%%%%%%%%%

Each C++ object also has an optional ``customization`` field. This allows for
additional customization of the Python object generated. For example, to
customize the Python name of a C++ class, the ``name`` sub-field can be used::

  classes:
    Foo:
      file: Foo.hpp
      customization:
        name: Foobar

The above would name the C++ class ``Foo`` as ``Foobar`` on the Python side.

If generating a python package that may have naming conflicts with another
package, the ``module local`` option can be turned on.  This is done simply
by specifying the ``module_local`` field in ``customization`` i.e. ::

  classes:
    Foo:
      file: Foo.hpp
      customization:
        module_local

For the total list of what can be customized, see the following table:

+-----------------------------+---------------------------------+
|     Customization Key       |          Description            |
+=============================+=================================+
|        module_local         | No "value" given, the presence  |
|                             | of the flag will trigger the    |
|                             | addition                        |
|                             +---------------------------------+
|                             | Example:                        |
|                             |                                 |
|                             |    customization:               |
|                             |      module_local               |
|                             |                                 |
+-----------------------------+---------------------------------+
|        name                 | A string to denote the class    |
|                             | on the Python side              |
|                             |                                 |
|                             +---------------------------------+
|                             | Example:                        |
|                             |                                 |
|                             |   customization                 |
|                             |     name: free_templ_add        |
|                             |                                 |
|                             |                                 |
+-----------------------------+---------------------------------+
|        custom_enum_vals     | Pairs of strings, the first     |
|                             | the name of the value in C++,   |
|                             | followed by the desired name    |
|                             | in Python                       |
|                             |                                 |
|                             +---------------------------------+
|                             | Example:                        |
|                             |    customization:               |
|                             |      custom_enum_vals:          |
|                             |        val1: One                |
|                             |        val2: Second             |
+-----------------------------+---------------------------------+

Code Customization
%%%%%%%%%%%%%%%%%%

In addition to individual object customization, the AutoPyBind11 tool has the
option allow the user to customize the resulting pybind11 code for each module.
This occurs when a YAML file is supplied as a ``CONFIG_INPUT`` to the
``autopybind11_add_module`` call or as a ``-cg`` path to the Python execution.

This YAML file

+-----------------------------+---------------------------------+
|     Customization Key       |          Description            |
+=============================+=================================+
| ignore_namespace_structure  | ``true`` to place all found     |
|                             |   objects at the module level   |
|                             | ``false`` will enforce the      |
|                             |   C++ namespace structure on the|
|                             |   Python Code                   |
|                             +---------------------------------+
|                             | Example:                        |
|                             | ignore_namespace_structure: true|
+-----------------------------+---------------------------------+
|        private_members      | ``False`` to bind getters and   |
|                             | setters as normal.              |
|                             | ``True`` to bind                |
|                             | the corresponding private member|
|                             | variables as public members     |
|                             |                                 |
|                             | See `Pybind11's Documentation`_ |
|                             |                                 |
|                             +---------------------------------+
|                             | Example:                        |
|                             |     private_members: true       |
+-----------------------------+---------------------------------+

pybind11 structure
^^^^^^^^^^^^^^^^^^

Additionally, the structure of the output pybind11 code can be altered on a
*per-module* basis.  Text blocks can be copied from

  https://gitlab.kitware.com/autopybind11/autopybind11/-/blob/master/autopybind11/text_blocks.py

and customized.

**Things of Note**:

1. The pipe, "|", character at the beginning of the text block is critical.
   This ensures the carriage returns/new lines to appear in the resultant code
   as AutoPyBind11 expects.
2. Objects enclosed in brackets "{}" should not be changed, these are variables
   replaced by the autopybind11 code.
3. Any brackets "{}" that should remain brackets in the C++ code should be doubled.

An example of the customization can be found here:
  https://gitlab.kitware.com/autopybind11/autopybind_skeleton/-/blob/drake/config.yml


.. _Templated Classes:

Templated Classes
#################

Templated classes are instantiated and bindings are generated based off the
instantiation types provided in the `wrapper_input.yml`_ .  These instantiation
strings are used to generate a ``wrapper.hpp`` file.  This file, written by
AutoPyBind11 and used as the sole input to CastXML, contains the C++ code which
instantiates the templated classes and functions.

Templated Class Names
%%%%%%%%%%%%%%%%%%%%%

Class names in Python are then associated with the class in a manner such as
'class_name_double'.  To produce a custom name in Python that can dynamically
denote a type, Python Lambda functions are used. In place of a string based
name, a lambda defines functionally the custom name and schema for indicating
type and location of type indicator.

A good example of this for classes with single and double template parameters
can be found at ``example/custom_names/wrapper_input.yaml``.

In order to have your lambda read by the yaml parser it must be defined
in ``wrapper_input.yml`` in the form:

'lambda <var name>: "any strings must have a double quote to be read as strings
and not literals" <rest of function>'
The '' is required to surround the entire lambda function.

If a lambda function is not optimal for use, a list of custom class names will
also be accepted in the form [custom name,different_custom_name]

The only requirements for this list is that there must be an equal number of
custom names to the number of type instatiations desired. If there is not, some
classes may not end up being wrapped.

Notes on Templates:
%%%%%%%%%%%%%%%%%%%
The argument a custom naming lambda accepts is a Python list consisting of
strings listing each type parameter in a template.
e.x.  `["double", "float", "int"]` in an instance where a templated class has
three parameters.

For an example of a correctly written object, see the directories below `Tests`.
Each folder contains a valid YAML file and is used when the test is executed.

Execution
---------

via CMake
##########

The recommended path for the usage of this tool is via CMake.

Setup
%%%%%

By utilizing the ``find_package`` utility, the created macros and functions can be
introduced into the environment.  This repository contains the
``AutoPyBind11Config.cmake`` file which is the targe of the following command::

  find_package(AutoPyBind11)

After this command is found, one additional command will be used to acquire pybind11 via the
``FetchContent`` module::

    autopybind11_fetch_build_pybind11()

Add Library
%%%%%%%%%%%

To add a library for the written pybind11 code, we use the ``autopybind11_add_module``
function.  This function does the work of setting custom commands to generate
the pybind11 code and adds a library to CMake which contains the pybind11 code., and then
This command should be written once for each ``wrapper_input.yml`` file in the repository.
The command has the following structure::

    autopybind11_add_module(<name> YAML_INPUT <path_to>/wrapper_input.yml
                        DESTINATION <path_to_output>
                        LINK_LIBRARIES <library_1> <library_2>
                        [CONFIG_INPUT <path_to>/config.yml])

Only the ``CONFIG_INPUT`` flag is optional.  The file given to that argument can be used
to customize the templates used to create the pybind11 CPP code.  The default templates
can be found in `text_blocks.py`_
An example of the function invocation is here::

    autopybind11_add_module("example" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                            DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                            LINK_LIBRARIES wrapper_example)

via Python
##########
This script assumes that Python 3.* is used.
The Python script has help for the arguments::

    $ python3 autopybind11.py -h
    usage: autopybind11.py [-h] [-o OUTPUT_DIR] -y YAML_PATH --module_name
                           MODULE_NAME [-g CASTXML_PATH] [-cg CONFIG_DIR]
                           [--no-generation] [-rs RSP_PATH] [-pm]
                           [--common_cpp_body_fmt COMMON_CPP_BODY_FMT]
                           [--class_info_body_fmt CLASS_INFO_BODY_FMT]
                           [--init_fun_signature_fmt INIT_FUN_SIGNATURE_FMT]
                           [--init_fun_forward_fmt INIT_FUN_FORWARD_FMT]
                           [--cppbody_fmt CPPBODY_FMT]
                           [--module_cpp_fmt MODULE_CPP_FMT]
                           [--member_func_fmt MEMBER_FUNC_FMT]
                           [--constructor_fmt CONSTRUCTOR_FMT]
                           [--member_func_arg_fmt MEMBER_FUNC_ARG_FMT]
                           [--public_member_var_fmt PUBLIC_MEMBER_VAR_FMT]
                           [--private_member_var_fmt PRIVATE_MEMBER_VAR_FMT]
                           [--member_reference_fmt MEMBER_REFERENCE_FMT]
                           [--overload_template_fmt OVERLOAD_TEMPLATE_FMT]
                           [--wrap_header_fmt WRAP_HEADER_FMT]
                           [--operator_fmt OPERATOR_FMT]
                           [--call_operator_fmt CALL_OPERATOR_FMT]
                           [--enum_header_fmt ENUM_HEADER_FMT]
                           [--enum_val_fmt ENUM_VAL_FMT]

    Args that start with '--' (eg. -o) can also be set in a config file (specified
    via -cg). The config file uses YAML syntax and must represent a YAML 'mapping'
    (for details, see http://learn.getgrav.org/advanced/yaml). If an arg is
    specified in more than one place, then commandline values override config file
    values which override defaults.

    optional arguments:
      -h, --help            show this help message and exit
      -o OUTPUT_DIR, --output OUTPUT_DIR
      -y YAML_PATH, --input_yaml YAML_PATH
                            Path to input YAML file of objects to process
      --module_name MODULE_NAME
                            Desired name of the output pybind11 module
      -g CASTXML_PATH, --castxml-path CASTXML_PATH
                            Path to castxml
      -cg CONFIG_DIR, --config-path CONFIG_DIR
                            config file path
      --no-generation, -n   Only print name of files to be generated
      -rs RSP_PATH, --input_response RSP_PATH
      -pm, --private_members
      --common_cpp_body_fmt COMMON_CPP_BODY_FMT
      --class_info_body_fmt CLASS_INFO_BODY_FMT
      --init_fun_signature_fmt INIT_FUN_SIGNATURE_FMT
      --init_fun_forward_fmt INIT_FUN_FORWARD_FMT
      --cppbody_fmt CPPBODY_FMT
      --module_cpp_fmt MODULE_CPP_FMT
      --member_func_fmt MEMBER_FUNC_FMT
      --constructor_fmt CONSTRUCTOR_FMT
      --member_func_arg_fmt MEMBER_FUNC_ARG_FMT
      --public_member_var_fmt PUBLIC_MEMBER_VAR_FMT
      --private_member_var_fmt PRIVATE_MEMBER_VAR_FMT
      --member_reference_fmt MEMBER_REFERENCE_FMT
      --overload_template_fmt OVERLOAD_TEMPLATE_FMT
      --wrap_header_fmt WRAP_HEADER_FMT
      --operator_fmt OPERATOR_FMT
      --call_operator_fmt CALL_OPERATOR_FMT
      --enum_header_fmt ENUM_HEADER_FMT
      --enum_val_fmt ENUM_VAL_FMT

This is useful for some small testing. But since a Python call would be needed to wrap each input file, we recommend
using the CMake system above.

Existing AutoPybind11 Usage
+++++++++++++++++++++++++++

The AutoPyBind11 tool has started to utilized in the process of generating
bindings for larger suites of software.  For examples of integrating
AutoPyBind11 into other build systems, see the following examples

Robot Locomotion/Drake
----------------------

* `CMake Integration`_
* `Bazel Integration`_

Additional links will be placed here.

Development and Contributing
++++++++++++++++++++++++++++

For someone who is going to contribute to the AutoPyBind11 repository, we have
included a set of  ``SetupForDevelopment`` scripts.  Each script will install the
`pre-commit`_ package and use it to install a pre-commit hook which runs the
`black`_ tool to automatically format the Python code found in the repository,
if possible.

We highly recommend that the environment-appropriate script be used before
development starts.

Style Checking / Linting
-------------------------

This repository uses the pycodestyle_ tool for linting and style checking which
can be installed via the ``pip`` program.
The program should be run at the top level so that the ``setup.cfg`` in the
repository is available. An example run is as follows::

  $ pycodestyle autopybind11/

or setup a CMake build system and execute the ``lint_lib`` test::

  $ ctest -R lint_lib -VV

Alternatives Comparison
-----------------------

*Note*: Some mentions here are purely observational, and do not yet have
explicit comparisons against ``autopybind11``.

CLIF
####

https://github.com/google/clif

Philosophically in some ways similar to hooking up CastXML (they use clang /
llvm as their C++ parser), a code generator (like this project), and a runtime
library for interfacing (like pybind11). This means mean most of the
glue that would need to be written is already there. It was originally written
as a general IDL (Interface Definition Language), which is great for
multi-language support (e.g. Python, MATLAB, etc), but will suffer when there
are language-specific foreign functional interfaces (FFIs).

However, it is an incomplete and currently dead project (from a public point of
view). Also, it has no Numpy and Eigen support.  It had a high profile
developer in Google and there were once some mentions of CLIF in the Bazel
codebase.

This project may possibly resume at some point
(`drake#7889 <https://github.com/RobotLocomotion/drake/issues/7889#issuecomment-634260874>`_).

Shibboken2
##########

* https://doc.qt.io/qtforpython/shiboken2/shibokengenerator.html
* https://doc.qt.io/qtforpython/shiboken2/considerations.html#general

This provides both the CPython interface and C++ API scanning (similar to
PyBindGen, CLIF, etc.), and is used in PySide, but is sufficiently general to
use in other projects.

The documentation is extremely comprehensive, and provides detailed
explanations of each step in the process, especially in sections where
customizations are necessary.

One drawback is that debugging will involve both generated CPython code as well
as the binding analysis itself, similar as with CLIF. (It's similar to
debugging ``pybind11``, but in ``pybind11`` the main hoop to jump through is
C++ template metaprogramming).

pybind11
########

.. note: This should be moved to the top.

https://pybind11.readthedocs.io/en/stable/

This is the "spiritual successor" to Boost.Python, and is a very popular Python
binding library.

Binding definitions are written in pure C++ code, which allows more familiarity
with developers, and slightly easier debugging (vs. interfaces like SWIG, where
you must debug both the binding generation *and* the runtime). pybind11 not
only provides a mechanism for exposing C++ to Python, but also provides its
Python C++ interface for exposing Python to C++.

pybind11 (but not Boost.Python) includes extensive builtin Numpy support with
C++ Eigen interface, ranging from interfacing with ``Eigen::Map<>`` to handling
sparse and dense arrays.

Some features are missing and have been added via patches with varying success
(in the author's opinion, the codebase may be difficult to understand).

pybind11 upstream is generally tentative about taking on major changes that
only benefit a few downstream projects, and at times can have a slow response
rate.

At present, there is an increasing trend of responses on the maintainer's part.

Several open source projects are already working on generating pybind11
bindings.

.. note::

  As mentioned above, this project emits pybind11 C++ code, and is meant to
  help minimize the minutae of writing high-quality bindings with pybind11.

SWIG
####

http://www.swig.org/

Multi-language support would be an advantage.  Projects in the open source
community already have code that generates SWIG bindings,
but the authors have stated that if they were starting now, they would use
pybind11.

The SWIG intermediate language can be seen as a boon when the interfaces are
simple, but a bottleneck when more complexity is necessary -- a standard issue
with IDLs aiming towards providing FFIs.

The developers of `Drake`_ previously used SWIG, but then transitioned to
pybind11 due to its extensive NumPy and Eigen support, and the ability to
(relatively easily) fork the code to provide support for dynamic scalar types.

.. _`Drake`: https://drake.mit.edu/

CPPWG
#####

https://github.com/jmsgrogan/cppwg/

CPPWG was the first tool used in the attempt to automate Python bindings in
Drake (`drake#7889 <https://github.com/RobotLocomotion/drake/issues/7889>`_).

This tool was the inspiration for the structure of the AutoPyBind11 system. It
uses
CastXML with pygccxml and nested configuration files to denote what classes and
free functions needed to be wrapped by the tool.  It also had a configurable set
of pybind11 output, but with much less customization opportunity as the
customization was limited to a few specific places in the code as opposed to
the structure of the pybind11 code which AutoPyBind11 allows.

The tool’s major shortcoming was the method’s handling of templated classes.
It used string matching while parsing the C++ source file to determine if
any of the lines found in the object required templating.  Additionally,
a test case was created which found an issue with the usage of templates
of wrapped code using a single instantiation type.

Binder
######

https://github.com/RosettaCommons/binder and https://cppbinder.readthedocs.io/en/latest/

Binder is another tool that performs the automatic wrapping of C++ code into
Python via pybind11. It uses C++ as the language of the tool, as opposed to
AutoPyBind11 which uses Python.  The Binder tool also uses a configuration file.
The system uses a “+/-” for inclusion and omission of the three basic objects:
namespaces, classes, and functions.  Binder’s configuration file can also alter
the functions used to bind the code.  The user can specify a function which is
run in place of the default binding code.

genpybind
#########

https://github.com/kljohann/genpybind

This uses the both ``clang`` C++ AST API and the ``clang.cindex`` Python AST
API to generate bindings ``pybind11``, similar to ``binder`` mentioned above.
However, the primary philosophy here is to annotate any additional information
(e.g. return value policies, renames, etc.) using explicit compiler attributes
using macros.

More information on why both the C++ and Python APIs are used are detailed
here: https://github.com/kljohann/genpybind#implementation

Interesting, the author would prefer to make a single C++ tool rather than a
single Python tool ;)

AutoWIG
#######

https://github.com/StatisKit/AutoWIG |
`Paper <https://arxiv.org/abs/1705.11000>`_ |
`Docs <https://autowig.readthedocs.io/en/latest/index.html>`_ |
`Example Code <https://github.com/StatisKit/FP17>`_

Provides a means to parse C++ code and emit either Boost.Python or pybind11
code, using ``clang.cindex`` in Python (Python bindings of C ``libclang`` API).
It can also translate docstrings from Doxygen to Sphinx, including symbol
references.

It also provides a comprehensive class structure with different passes, which
look great for generalization, but may also cause developers some pain with
indirection via abstraction.

It's an impressive project, and the paper (cited above) has some interesting
comparisons (including a comparison of methods for VTK and ITK). However,
the current development is a bit unclear. The documentation seems most
comprehensive in the arXiv publication, but appears lacking in the ReadTheDocs
website.

PyBindGen
#########

https://pybindgen.readthedocs.io/en/latest/ and https://github.com/gjcarneiro/pybindgen

PyBindGen is a Python-only module that allows the specification of C++ modules
to wrap into custom Python runtime (not pybind11 code!).

It doesn’t rely on a configuration file to determine what parts of the C++ code
are wrapped but instead uses a Python script to
create the modules and add all objects.  The compilation of the resultant code
is done via a “python setup.py” command and is what creates the C++ code that
calls into the CPython API.

It does have some pygccxml integration.  One mode of execution uses pygccxml to
parse C++ code and write the pybind11 code into a separate file.  The other
mode parses the header files but writes out the information into a PyBindGen
script.

Most notably, because this code generates its own bindings, any runtime
debugging must done jointly with the code generation, which would incur similar
overhead like debugging errors with SWIG.

cppyy
#####

* https://cppyy.readthedocs.io/en/latest/index.html
* https://bitbucket.org/wlav/cppyy/src/master/
* https://bitbucket.org/wlav/cppyy-backend/src/master/clingwrapper/
* https://bitbucket.org/wlav/cppyy-backend/src/master/cling/

  - https://bitbucket.org/wlav/cppyy-backend/src/master/cling/python/cppyy_backend/

Generates Python bindings of C++ code on the fly using Cling (which leverages
the clang frontend and LLVM backend).

The PyHPC paper in the above mentioned links cite `pygccxml` and `Reflex` as
motivating examples. The goal is to provide (interactive) bindings for C++
projects with APIs the size of ROOT. (Additionally, the paper has normalize
benchmarking, which is an excellent goal in any project!)

For an overview of the package structure, see
`the docs <https://cppyy.readthedocs.io/en/latest/packages.html#package-structure>`_.
Note that the ``cppyy_backend``
Python module actually accesses ``clang.cindex`` to provide the
``cppyy-generator`` tool used in its workflow:
`cppyy: Utilities <https://cppyy.readthedocs.io/en/latest/utilities.html#bindings-collection>`_.

Due to usage of Cling and the philosophy of lazy binding, user experience may
be heavily impacted at load time and when heavy new instantiations are
necessary (similar to complaintgs about Julia startup time).

However, these should generally only apply to projects with template-heavy
public APIs (e.g. those that use Eigen). Performance could possible be tuned.
For more details, see this GitHub discussion:
`drake#7889 (comment)
<https://github.com/RobotLocomotion/drake/issues/7889#issuecomment-663721506>`_.

Other Mentions
##############

* https://github.com/robotpy/robotpy-build

  - Only for RobotPy
  - Emits pybind11 code
  - Uses `header2whatever <https://pypi.org/project/header2whatever/>`_

.. _pycodestyle: https://pypi.org/project/pycodestyle/
.. _Copyright.txt: Copyright.txt
.. _`data.kitware.com`: https://data.kitware.com/#folder/57b5de948d777f10f2696370
.. _`text_blocks.py`: text_blocks.py
.. _PYPI: https://pypi.org/project/castxml/
.. _black: https://black.readthedocs.io/en/stable/
.. _`pre-commit`: https://pre-commit.com/
.. _`Pybind11's documentation`: https://pybind11.readthedocs.io/en/stable/classes.html#instance-and-static-fields
.. _`Bazel Integration`: https://github.com/RobotLocomotion/drake/pull/14265
.. _`CMake Integration`: https://gitlab.kitware.com/autopybind11/autopybind_skeleton/-/tree/drake