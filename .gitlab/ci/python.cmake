cmake_minimum_required(VERSION 3.12)

set(data_host "https://www.paraview.org")
set(file_item "python-win64-3.9.0.tar.xz")
set(file_hash "858061fbfb0a53387c1278a535bcd4abeae4ab07187be6042bfb02ca222c229f")

# Download the file.
file(DOWNLOAD
  "${data_host}/files/dependencies/${file_item}"
  ".gitlab/${file_item}"
  STATUS download_status
  EXPECTED_HASH "SHA256=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download ${file_item}: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xzf ".gitlab/${file_item}"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract ${file_item}: ${err}")
endif ()
