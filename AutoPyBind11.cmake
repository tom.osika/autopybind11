# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

macro(autopybind11_fetch_build_pybind11)
  set(oneValueArgs "PYBIND11_DIR")
  cmake_parse_arguments(APB "" "${oneValueArgs}" "" ${ARGN} )

  find_package(Python COMPONENTS Interpreter Development)

  # Sets the above Python version to the "Legacy" variable for the executable
  # which will be set by the calls in PyBind11
  set(PYTHON_EXECUTABLE ${Python_EXECUTABLE} CACHE FILEPATH "Path to the Python executable to use.")

  # if pybind11 source dir is specified add it
  if(DEFINED APB_PYBIND11_DIR)
    if(NOT TARGET pybind11::pybind11)
      # Without a target of pybind11 already, assume this is path to
      # repository and can be built again.
      add_subdirectory("${APB_PYBIND11_DIR}" ${CMAKE_CURRENT_BINARY_DIR}/pybind11)
    endif()
  else()
    # if pybind11 source dir is not specified download it
    include(FetchContent)
    FetchContent_Declare(
      pybind11
      GIT_REPOSITORY https://github.com/pybind/pybind11
      GIT_TAG        v2.5.0
      )
    set(FETCHCONTENT_QUIET OFF)
    FetchContent_MakeAvailable(pybind11)
  endif()
  include_directories(${Python_INCLUDE_DIRS})

  find_program(CastXML_EXECUTABLE NAMES castxml castxml.exe REQUIRED)
  execute_process(
    COMMAND
         ${CastXML_EXECUTABLE}
         --version
      OUTPUT_VARIABLE castxmlVersion
      OUTPUT_STRIP_TRAILING_WHITESPACE)
  # Check for a CastXML version from the relatively recent timeframe.
  # May want to be more specific when Comment capabilities are added.
  string(REGEX MATCH "castxml version 0\.3\.[0-9]+" castxmlVersionCheck ${castxmlVersion})
  if(NOT castxmlVersionCheck)
    message(FATAL_ERROR "Version of CastXML is incompatible.  Please install CastXML 0.3.* by PIP
    or download a version from https://data.kitware.com/#folder/57b5de948d777f10f2696370")
  endif()
  set(AutoPyBind11_generator ${PYTHON_EXECUTABLE} -m autopybind11)
endmacro()

function(_autopybind11_get_compiler_flags)
    set(generateSingleValueArgs INCLUDE_FLAGS DEFINE_FLAGS INDEX_FLAGS)
    set(generateMultiValueArgs TARGETS INCLUDE_DIRS)
    cmake_parse_arguments(PYBIND_GET_FLAGS "" "${generateSingleValueArgs}" "${generateMultiValueArgs}" ${ARGN})
    list(LENGTH PYBIND_GET_FLAGS_TARGETS num_targets)
    list(LENGTH PYBIND_GET_FLAGS_INCLUDE_DIRS num_inc_dirs)
    set(includes_list)
    set(defines_list)
    set(indexes_list)
    if(${num_targets} GREATER 0)
        foreach(target ${PYBIND_GET_FLAGS_TARGETS})
          list(APPEND
               includes_list
               $<TARGET_PROPERTY:${target},INTERFACE_INCLUDE_DIRECTORIES>)

          list(APPEND
               defines_list
               $<TARGET_PROPERTY:${target},INTERFACE_COMPILE_DEFINITIONS>)
          list(APPEND indexes_list $<TARGET_PROPERTY:${target},_indexes>)
        endforeach()
        set(${PYBIND_GET_FLAGS_INCLUDE_FLAGS} "${includes_list}"  PARENT_SCOPE)
        set(${PYBIND_GET_FLAGS_DEFINE_FLAGS} "${defines_list}" PARENT_SCOPE)
        set(${PYBIND_GET_FLAGS_INDEX_FLAGS} "${indexes_list}" PARENT_SCOPE)
    endif()

    if(${num_inc_dirs} GREATER 0)
      foreach(dir ${PYBIND_GET_FLAGS_INCLUDE_DIRS})
        list(APPEND includes_list ${dir})
      endforeach()
      set(${PYBIND_GET_FLAGS_INCLUDE_FLAGS} "${includes_list}"  PARENT_SCOPE)
    endif()
endfunction()

function(_autopybind11_get_sources)
    set(PreProcessOneValueArgs YAML_INPUT CONFIG_INPUT DESTINATION GENERATED_SOURCES GENERATED_INDEXES)
    set(PYBIND_RUN_NAME ${ARGV0})
    cmake_parse_arguments(PARSE_ARGV 1 PYBIND_RUN "" "${PreProcessOneValueArgs}" "")
    if(PYBIND_RUN_CONFIG_INPUT AND EXISTS ${PYBIND_RUN_CONFIG_INPUT})
      set(CONFIG_FLAGS "-cg" "${PYBIND_ADD_LIB_CONFIG_INPUT}")
    endif()
    execute_process(
      COMMAND
         ${AutoPyBind11_generator}
         "-y"  ${PYBIND_RUN_YAML_INPUT}
         "-o"  ${PYBIND_RUN_DESTINATION}
         "-g"  ${CastXML_EXECUTABLE}
         ${CONFIG_FLAGS}
         "--module_name" "${PYBIND_RUN_NAME}"
         "-s" "1"
      OUTPUT_VARIABLE sources
      OUTPUT_STRIP_TRAILING_WHITESPACE)
    list(GET sources 0 outfiles)
    list(GET sources 1 indxfiles)
    string(REPLACE "%" ";" outfiles ${outfiles})
    string(REPLACE "%" ";" indxfiles ${indxfiles})
    set(${PYBIND_RUN_GENERATED_SOURCES} ${outfiles} PARENT_SCOPE)
    set(${PYBIND_RUN_GENERATED_INDEXES} ${indxfiles} PARENT_SCOPE)
endfunction()

function(autopybind11_add_module)
    if(NOT DEFINED AutoPyBind11_generator)
      message(FATAL_ERROR "autopybind11_fetch_build_pybind11 must be called first")
    endif()
    set(generateOneValueArgs YAML_INPUT CONFIG_INPUT DESTINATION NAMESPACE C_STD_FLAG GEN_ONLY)
    set(generateMultiValueArgs LINK_LIBRARIES FILES INCLUDE_DIRS)
    set(PYBIND_ADD_LIB_NAME ${ARGV0})

    cmake_parse_arguments(PARSE_ARGV 1 PYBIND_ADD_LIB "" "${generateOneValueArgs}" "${generateMultiValueArgs}")
    if(NOT PYBIND_ADD_LIB_YAML_INPUT)
      message(FATAL_ERROR "YAML_INPUT must be specified")
    endif()
    if(NOT PYBIND_ADD_LIB_LINK_LIBRARIES)
      message(FATAL_ERROR "LINK_LIBRARIES must be specified")
    endif()
    if(NOT PYBIND_ADD_LIB_C_STD_FLAG)
      set(PYBIND_ADD_LIB_C_STD_FLAG "-std=c++14")
    endif()
    _autopybind11_get_sources(
      ${PYBIND_ADD_LIB_NAME}
        YAML_INPUT ${PYBIND_ADD_LIB_YAML_INPUT}
        CONFIG_INPUT ${PYBIND_ADD_LIB_CONFIG_INPUT}
        DESTINATION ${PYBIND_ADD_LIB_DESTINATION}
        GENERATED_SOURCES PYBIND_ADD_LIB_FILES
        GENERATED_INDEXES PYBIND_ADD_LIB_INDXS)
    if (PYBIND_ADD_LIB_NAMESPACE)
      set(DEFAULT_NAMESPACE "-d" "${PYBIND_ADD_LIB_NAMESPACE}")
    endif()
    _autopybind11_get_compiler_flags(
      TARGETS
        ${PYBIND_ADD_LIB_LINK_LIBRARIES}
      INCLUDE_DIRS 
        ${PYBIND_ADD_LIB_INCLUDE_DIRS}
      INCLUDE_FLAGS includes
      DEFINE_FLAGS  defines
      INDEX_FLAGS indexes)

    # Get name of file which should correspond directly to name of python module created in it
    # The first name in the list should be the overall module
    list(GET PYBIND_ADD_LIB_FILES 0 file)
    get_filename_component(tgt_helper_name ${file} NAME_WE)

    # Add custom target to generate the code
    # Add depends on wrapper_input.YAML and header files?

    # Generate file
    set(PYBIND_ADD_LIB_RESPONSE_PATH "${CMAKE_CURRENT_BINARY_DIR}/response.rsp")

    file(GENERATE OUTPUT "${PYBIND_ADD_LIB_RESPONSE_PATH}"
      CONTENT "includes: ${includes}\nc_std: ${PYBIND_ADD_LIB_C_STD_FLAG}\ndefines: ${defines}\nindex_files: ${indexes}")

    if(PYBIND_ADD_LIB_CONFIG_INPUT AND EXISTS ${PYBIND_ADD_LIB_CONFIG_INPUT})
        add_custom_command(
          OUTPUT
            ${PYBIND_ADD_LIB_FILES}
            wrapper.cpp
          COMMAND
            ${AutoPyBind11_generator}
              -y ${PYBIND_ADD_LIB_YAML_INPUT}
              "--module_name" ${PYBIND_ADD_LIB_NAME}
              "-g"  ${CastXML_EXECUTABLE}
              "-cg" "${PYBIND_ADD_LIB_CONFIG_INPUT}"
              "-rs" "${PYBIND_ADD_LIB_RESPONSE_PATH}"
              "-o"  ${PYBIND_ADD_LIB_DESTINATION}
              "-s" "2"
              ${DEFAULT_NAMESPACE}
          DEPENDS
            ${PYBIND_ADD_LIB_YAML}
            ${PYBIND_ADD_LIB_INDXS}
            ${PYBIND_ADD_LIB_CONFIG_INPUT}
            ${PYBIND_ADD_LIB_RESPONSE_PATH}
          WORKING_DIRECTORY ${PYBIND_ADD_LIB_DESTINATION})

    else()
      add_custom_command(
        OUTPUT
          ${PYBIND_ADD_LIB_FILES}
          wrapper.cpp
        COMMAND
          ${AutoPyBind11_generator}
            -y ${PYBIND_ADD_LIB_YAML_INPUT}
            "--module_name" ${PYBIND_ADD_LIB_NAME}
            "-g"  ${CastXML_EXECUTABLE}
            "-o"  ${PYBIND_ADD_LIB_DESTINATION}
            "-rs" ${PYBIND_ADD_LIB_RESPONSE_PATH}
            "-s" "2"
            ${DEFAULT_NAMESPACE}
          DEPENDS
            ${PYBIND_ADD_LIB_YAML}
            ${PYBIND_ADD_LIB_RESPONSE_PATH}
          WORKING_DIRECTORY ${PYBIND_ADD_LIB_DESTINATION})
    endif(PYBIND_ADD_LIB_CONFIG_INPUT AND EXISTS ${PYBIND_ADD_LIB_CONFIG_INPUT})

    # If we're only generating the bindings, not compiling...
    # add_custom_target doesn't compile but allows sources to be found in parent scope
    # via the custom_target properties:
    #     get_target_property(APB_GENERATED_FILES ${NAME} SOURCES)
    # Target is added to all to accommodate stand-alone builds (i.e. no other targets than
    # ones created by autopybind11).
    if(PYBIND_ADD_LIB_GEN_ONLY)
      add_custom_target(${tgt_helper_name}_files ALL SOURCES ${PYBIND_ADD_LIB_FILES})
      return()
    endif()

    # Else, create the module with that file as it's only source
    pybind11_add_module(
      ${tgt_helper_name}
      SHARED
        ${PYBIND_ADD_LIB_FILES})

    target_link_libraries(
      ${tgt_helper_name}
      PUBLIC
        ${PYBIND_ADD_LIB_LINK_LIBRARIES})

    set_target_properties(
      ${tgt_helper_name}
      PROPERTIES
        "_indexes"
        "${PYBIND_ADD_LIB_INDXS}")

    target_include_directories(
      ${tgt_helper_name}
      PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${PYBIND_ADD_LIB_DESTINATION}
        ${PYBIND_ADD_LIB_INCLUDE_DIRS})
endfunction()
