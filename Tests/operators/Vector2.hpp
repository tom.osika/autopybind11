/*
  Copyright (c) 2016 Wenzel Jakob <wenzel.jakob@epfl.ch>
  All rights reserved. Use of this source code is governed by a
  BSD-style license that can be found in the LICENSE file.
  Copyright (c) 2016 Wenzel Jakob <wenzel.jakob@epfl.ch>, All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Please also refer to the file CONTRIBUTING.md, which clarifies licensing of
  external contributions to this project including patches, pull requests, etc.
*/
#include <string>

// TODO: delete after testing
// void temp1(){}
// template <typename T>
// T temp2(){ return T(0); }


class Vector2 {
public:
  Vector2(float x, float y) : x(x), y(y) { }
  std::string toString() const { return "[" + std::to_string(x) + ", " + std::to_string(y) + "]"; }
  float get_x() { return x; }
  float get_y() { return y; }

  // Unary + and -
  Vector2 operator+() const { return *this; }
  Vector2 operator-() const { return Vector2(-x, -y); }

  // Non-unary
  Vector2 operator+(const Vector2 &v) const { return Vector2(x + v.x, y + v.y); }
  Vector2 operator-(const Vector2 &v) const { return Vector2(x - v.x, y - v.y); }
  Vector2 operator-(float value) const { return Vector2(x - value, y - value); }
  Vector2 operator+(float value) const { return Vector2(x + value, y + value); }
  Vector2 operator*(float value) const { return Vector2(x * value, y * value); }
  Vector2 operator/(float value) const { return Vector2(x / value, y / value); }
  Vector2 operator*(const Vector2 &v) const { return Vector2(x * v.x, y * v.y); }
  Vector2 operator/(const Vector2 &v) const { return Vector2(x / v.x, y / v.y); }
  Vector2& operator+=(const Vector2 &v) { x += v.x; y += v.y; return *this; }
  Vector2& operator-=(const Vector2 &v) { x -= v.x; y -= v.y; return *this; }
  Vector2& operator*=(float v) { x *= v; y *= v; return *this; }
  Vector2& operator/=(float v) { x /= v; y /= v; return *this; }
  Vector2& operator*=(const Vector2 &v) { x *= v.x; y *= v.y; return *this; }
  Vector2& operator/=(const Vector2 &v) { x /= v.x; y /= v.y; return *this; }
  float operator()() { return x + y; }
  float operator()(float val1) { return x + y + val1; }
  float operator()(float val1, float val2) { return x + y + val1 + val2; }
  bool operator==(const Vector2 &v)
  {
    return this -> x == v.x &&
           this -> y == v.y;
  }

  bool operator!=(const Vector2 &v) { return !(*this == v); }

private:
  float x, y;
};
