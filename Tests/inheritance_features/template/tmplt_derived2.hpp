/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef TMPLT_DERIVED2_HPP
#define TMPLT_DERIVED2_HPP

#include "tmplt_derived1.hpp"
#include <string>

namespace nmspc1 {
namespace nmspc2 {

template <typename R, typename S>
class TD2 : public TD1<R>
{
protected:
  class protectedClass
  {
    public:
        protectedClass() = default;
        ~protectedClass() = default;
        int val = 1;
  };

  protectedClass protectedFn()
  {
    return protectedClass();
  };
public:
  S var2;

  inline TD2() { var2 = S(0); }
  inline virtual std::string whoami() const override
  {
    return std::string("TD2");
  }

  inline virtual std::string virt2(float f, std::string s) const
  {
    return std::string("TD2.virt2()");
  }

  // Note that we are relying on TD1's default implementation
  // of virt1(). Making sure that inherited functions that don't appear in
  // the derived class's definition are still overridable/present in the
  // trampoline.
};

template <typename R, typename S>
inline std::string call_virt_from_td2(const TD2<R, S>& td2)
{
  std::string ret;
  ret += td2.whoami();
  ret += ": ";
  ret += td2.virt1(3.14);
  ret += td2.virt2(-3.14, std::string("a_string"));
  return ret;
}
}}
#endif