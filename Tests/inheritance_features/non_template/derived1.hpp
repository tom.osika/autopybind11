/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef DERIVED1_HPP
#define DERIVED1_HPP

#include "base.hpp"
#include <string>

namespace nmspc1 {

class Derived1 : public Base
{
public:
  int var1;

  inline Derived1() { var1 = 0; }
  inline virtual std::string whoami() const override
  {
    return std::string("Derived1");
  }

  // In each subclass, we'll implement a virtual function
  // with a name corresponding to where the class is in the hierarchy
  // Derived1 implements virt1, Derived2 implements virt2.
  // These will only be implemented in the original class and inherited
  // everywhere else.
  inline virtual std::string virt1(float f) const
  {
    return std::string("Derived1.virt1()");
  }

protected:
  virtual std::string prot_virt_fxn() const override
  {
    return std::string("Derived1.prot_virt_fxn()");
  }

private:
  // Make sure private non-pure virtual functions aren't included in trampoline
  inline virtual std::string priv_virt_fxn() const override
  {
    return std::string("Derived1.priv_virt_fxn()");
  }
};

inline std::string call_virt_from_derived1(const Derived1& d1)
{
  std::string ret;
  ret += d1.whoami();
  ret += ": ";
  ret += d1.virt1(3.14);
  return ret;
}
}
#endif