
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

void apb11_smoke_test_A_py_register(py::module& model);
void apb11_smoke_test_B_py_register(py::module& model);

PYBIND11_EXPORT void apb11_smoke_test_register_types(py::module& model) {
  // make sure this module is only initialized once
  static bool called = false;
  if (called) {
    return;
  }
  called = true;

  // initialize class for module smoke_test
  apb11_smoke_test_A_py_register(model);
  apb11_smoke_test_B_py_register(model);
};

PYBIND11_MODULE(smoke_test, model) {
  // First initialize dependent modules
  // then this module.
  apb11_smoke_test_register_types(model);
}
