#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "${input_dir}/sample.hpp"

namespace py = pybind11;
void apb11_smoke_test_B_py_register(py::module& m) {
  static bool called = false;
  if (called) {
    return;
  }
  called = true;
  py::class_<::B> B(m, "B");

  B.def(py::init<>())
      .def(py::init<::B const&>(), py::arg("arg0"))
      .def("Something", static_cast<::A (::B::*)() const>(&::B::Something))

      ;
}
