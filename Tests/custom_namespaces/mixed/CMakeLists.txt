add_library(custom_mixed_namespace INTERFACE)
target_sources(custom_mixed_namespace INTERFACE
  ${CUSTOM_NAMESPACE_INCLUDE_DIR}/no_namespace.hpp
  ${CUSTOM_NAMESPACE_INCLUDE_DIR}/nested_names.hpp)
target_include_directories(custom_mixed_namespace INTERFACE
  ${CUSTOM_NAMESPACE_INCLUDE_DIR})

autopybind11_add_module("custom_mixed_namespace_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES custom_mixed_namespace
                       )