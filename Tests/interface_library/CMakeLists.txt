cmake_minimum_required(VERSION 3.15)
project(interface_library CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})
add_library(interfaceLib INTERFACE)
target_sources(interfaceLib INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/test.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/test_2.hpp)
target_include_directories(interfaceLib INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("interfaceWrap" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/interface_library_wrap.yml
                        CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES interfaceLib)
