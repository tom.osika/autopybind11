# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest
# Make it so we search where we are running.
sys.path.append(os.getcwd())

import myModule as mm


class ExtraIncludeDirTests(unittest.TestCase):
    def test_constructors(self):
        mm.better_printer()
        mm.better_printer("foo")

    def test_method(self):
        bp = mm.better_printer("AutoPyBind11")
        bp.better_print()

if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)