import deleted_constructor_module

try:
    deleted_constructor_module.FormulaCell()
except TypeError:
    print("Simple: deleted constructor throws proper type error")
    exit(0)
