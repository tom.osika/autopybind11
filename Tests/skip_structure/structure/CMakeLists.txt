add_library(simpleLib_2 simple_structure.cxx)
set_property(TARGET simpleLib_2 PROPERTY POSITION_INDEPENDENT_CODE ON)
autopybind11_add_module("enforce_structure" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/simple_wrap.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES simpleLib_2)
