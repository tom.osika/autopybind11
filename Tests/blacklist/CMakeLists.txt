# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt                                              
cmake_minimum_required(VERSION 3.15)
project(blacklist)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})
add_library(blacklist blacklist.cxx)
set_property(TARGET blacklist PROPERTY POSITION_INDEPENDENT_CODE ON)
autopybind11_add_module("blacklistTest" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES blacklist)
