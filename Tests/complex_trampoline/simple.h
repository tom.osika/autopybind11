#ifndef simple_h
#define simple_h

#include <map>

template <typename T>
class customObj
{

};
class simple_base
{
  protected:
    virtual ::std::map<double,double> test() =0;
};

class simple
{
public:
  simple();
  int hello();
protected:
    ::std::map<double,double> test();
};

#endif
