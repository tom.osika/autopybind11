/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef MODULE1_HPP
#define MODULE1_HPP

// #include "../test_2.hpp"
// Future work to properly inherit from other modules.
// class inherited : public named::cross_target {};

template < typename T = double >
class adl_test_base
{
public:
  T base_var;
  inline adl_test_base() {};
  inline T mult(const T& val) {return base_var * val; };
};

class nonTemplate
{
public:
  nonTemplate();
  double div(double v1, double v2);
  float fdiv(float v1, float v2);
};

#endif
