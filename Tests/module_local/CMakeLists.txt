# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(module_local CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
add_subdirectory(math_module)
add_subdirectory(sci_module)
