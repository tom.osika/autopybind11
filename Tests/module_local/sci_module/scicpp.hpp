/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */

#ifndef SCICPP_HPP
#define SCICPP_HPP
#include <vector>
namespace sci{
   
    class vector_addition
    {
    public:
        vector_addition() {};
        //elementwise vector addition
        std::vector<int> add_vecs(std::vector<int> a, std::vector<int> b);
    };
}

class adder
{
public:
    adder() : val1(0), val2(0) {};
    adder(int i, int j);
    void setter(int i, int j);
    int sum();
private:
    int val1;
    int val2;
};

#endif //SCICPP_HPP