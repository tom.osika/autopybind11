/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */

#include "scicpp.hpp"

adder::adder(int i, int j)
{
    val1 = i;
    val2 = j;
};

void adder::setter(int i, int j)
{
    val1 = i;
    val2 = j;
};

int adder::sum()
{
    return val1+val2;
};


std::vector<int> sci::vector_addition::add_vecs(std::vector<int> a, std::vector<int> b)
{
    std::vector<int> ret_vec = std::vector<int>();
    adder add = adder();
    for(int i = 0; i < a.size(); i++)
    {
        add.setter(a[i], b[i]);
        ret_vec.push_back(add.sum());
    }
    return ret_vec;
}